# Data manager library

[![Maven Central](https://img.shields.io/maven-central/v/net.relaysoft.commons/data-manager-spring-boot-starter.svg?style=flat-square)](http://search.maven.org/#artifactdetails%7Cnet.relaysoft.commons%7Cdata-manager-spring-boot-starter%7C3.0.0%7Cjar)

## Overview

Spring Boot starter library for the [data-manager](https://gitlab.com/iRelay/data-manager) project. Enables Spring Boot style auto 
configuration for the data service to provide it as bean component.

By default without any configurations data service is configured with in-memory data manager and simple cache and security services. Refer
[data-manager](https://gitlab.com/iRelay/data-manager) project guide for configuring build-in components.

It is possible to use Spring Boot component based data manager implementation with the auto configured service. Most simple use case is to
Create data manager bean component and auto configuration will automatically attach it to data service bean. It is also possible to define
data manager component with qualified name and configure Spring Boot application to use data manager bean component based on name or class
name set in `application.properties` or `application.yml`. Use property with name `relaysoft.data.manager.name`.

Furthermore you can provide custom implementations of data cache and security services by overriding protected getters in data manager bean
implementation. By default all data managers extend from `AbstractDataManager` will use simple in-memory cache and security services.

## Example

Attach data Manager Spring Boot Starter into your project as Maven dependency.

```xml
<dependency>
	<groupId>net.relaysoft.commons</groupId>
	<artifactId>data-manager-spring-boot-starter</artifactId> 
	<version>3.0.0</version>
</dependency>
```

Configure data manager name to use into `application.properties`.

```
relaysoft.data.manager.name = file
```

Refer [data-manager](https://gitlab.com/iRelay/data-manager) projects examples for the available configuration options.

Using auto configured data service in application components:

```java
@Service
public class SomeService {

@Autowired
private DataService dataService;

// Your implementations

public void doSomethingWithDataService() {
	// Do something
	Data data = dataService.createByteData(byte[] array, DataPersistenceEnum.CACHED);
	// Do something else
}
```

### Using custom data manager bean

You can also create and use custom data manager bean component and use it with default service. If only one data manager bean exists in classpath, then it is no need for configuring data manager name in `application.properties`. 

Example custom data manager bean:

```java
@Component
public class MyDataManager extends AbstractDataManager implements DataManager {

@Autowired
private DataCacheService cacheService;
	
@Autowired
private DataSecurityService securityService;

// Implement all needed methods

@Override
protected DataCacheService getDataCacheService() {
	return cacheService;
}
	
@Override
protected DataSecurityService getDataSecurityService() {
	return securityService;
}
```

It also possible to create and use custom `DataCacheService` and  `DataSecurityService` bean components. Just inject them into your custom data manager instead of default ones and override to protected `getDataCacheService()` and `getDataSecurityService()`.

## Getting Started with development

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 
See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Java version 8

### Installing

Just checkout sources from GitLab.

## Running the tests

Project unit tests are executed within Maven _test_.

Unit tests:

	mvn test

## Deployment

Project can be added as a part of existing Spring Boot application as Maven dependency.

```xml
<dependency>
	<groupId>net.relaysoft.commons</groupId>
	<artifactId>data-manager-spring-boot-starter</artifactId>
	<version>3.0.0</version>
</dependency>
```

Data service bean is automatically configured by Spring Boot.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on the process for submitting pull requests to us.

## Versioning

Uses [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/irelay/data-manager-spring-boot-starter/tags). 

## Authors

* **Tapani Leskinen** - [relaysoft.net](https://relaysoft.net)

## License

This project is licensed under the Apache License 2.0 - see the [LICENSE](LICENSE) file for details
