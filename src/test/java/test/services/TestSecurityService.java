package test.services;

import java.util.Properties;

import org.springframework.stereotype.Component;

import net.relaysoft.commons.data.services.SimpleDataSecurityService;

@Component
public class TestSecurityService extends SimpleDataSecurityService{
	
	private Properties properties;
	
	public TestSecurityService(Properties properties) {
		super(properties);
		this.properties = properties;
	}
	
	public Properties getProperties() {
		return properties;
	}
}
