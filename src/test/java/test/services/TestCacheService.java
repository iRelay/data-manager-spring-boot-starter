package test.services;

import java.util.Properties;

import org.springframework.stereotype.Component;

import net.relaysoft.commons.data.services.SimpleInMemoryDataCacheService;

@Component
public class TestCacheService extends SimpleInMemoryDataCacheService {
	
	private Properties properties;
	
	public TestCacheService(Properties properties) {
		super(properties);
		this.properties = properties;
	}
	
	public Properties getProperties() {
		return properties;
	}
}
