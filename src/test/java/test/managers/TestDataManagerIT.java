package test.managers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.relaysoft.commons.data.manager.DataManager;
import net.relaysoft.commons.data.manager.InMemoryDataManager;
import net.relaysoft.commons.data.services.DataCacheService;
import net.relaysoft.commons.data.services.DataSecurityService;

@Component("TestDataManager")
public class TestDataManagerIT extends InMemoryDataManager implements DataManager{

	@Autowired
	private DataCacheService cacheService;
	
	@Autowired
	private DataSecurityService securityService;
	
	public TestDataManagerIT() {
		super();
	}
	
	public DataCacheService getDataCacheServiceTest() {
		return getDataCacheService();
	}
	
	public DataSecurityService getDataSecurityServiceTest() {
		return getDataSecurityService();
	}
	
	@Override
	protected DataCacheService getDataCacheService() {
		return cacheService;
	}
	
	@Override
	protected DataSecurityService getDataSecurityService() {
		return securityService;
	}
}
