package test.managers;

import org.springframework.stereotype.Component;

import net.relaysoft.commons.data.manager.DataManager;
import net.relaysoft.commons.data.manager.InMemoryDataManager;
import net.relaysoft.commons.data.services.DataCacheService;
import net.relaysoft.commons.data.services.DataSecurityService;

@Component
public class TestDataManager extends InMemoryDataManager implements DataManager{

	public TestDataManager() {
		super();
	}
	
	public TestDataManager(int instanceId, DataCacheService cacheService, DataSecurityService securityService) {
		super(instanceId, cacheService, securityService);
	}
	
	public DataCacheService getDataCacheService() {
		return super.getDataCacheService();
	}
	
	public DataSecurityService getDataSecurityService() {
		return super.getDataSecurityService();
	}
	
	
}
