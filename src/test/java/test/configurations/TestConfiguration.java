package test.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;

import net.relaysoft.commons.data.conf.DataManagerProperties;

public class TestConfiguration {
	
	@Bean
    @ConfigurationProperties(prefix="relaysoft.data.manager")
    public DataManagerProperties settings (){
        return new DataManagerProperties();
    }
	
}
