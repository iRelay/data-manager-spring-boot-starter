package net.relaysoft.commons.data.conf;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.relaysoft.commons.data.conf.DataManagerProperties.Cache;
import net.relaysoft.commons.data.conf.DataManagerProperties.Security;
import test.configurations.TestConfiguration;

@TestPropertySource(locations="classpath:application.properties")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { TestConfiguration.class })
@EnableConfigurationProperties
public class DataManagerPropertiesTest {

	@Autowired
	private DataManagerProperties dataManagerProperties;

	@Test
	public void testGetCache() {
		Cache cache = dataManagerProperties.getCache();
		assertNotNull(cache);
		assertEquals("TestCacheService", cache.getName());
	}

	@Test
	public void testGetInstanceId() {
		assertEquals("1414", dataManagerProperties.getInstanceId());
	}
	
	@Test
	public void testGetLockTimeout() {
		assertEquals("4000", dataManagerProperties.getLockTimeout());
	}

	@Test
	public void testGetName() {
		assertEquals("TestDataManager", dataManagerProperties.getName());
	}

	@Test
	public void testGetSecurity() {
		Security security = dataManagerProperties.getSecurity();
		assertNotNull(security);
		assertEquals("TestSecurityService", security.getName());
	}

}
