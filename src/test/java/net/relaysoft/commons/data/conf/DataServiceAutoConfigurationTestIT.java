package net.relaysoft.commons.data.conf;

import static org.junit.Assert.*;

import java.lang.reflect.Field;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.relaysoft.commons.data.manager.DataManager;
import net.relaysoft.commons.data.services.AbstractDataService;
import net.relaysoft.commons.data.services.DataCacheService;
import net.relaysoft.commons.data.services.DataSecurityService;
import net.relaysoft.commons.data.services.DataService;
import test.managers.TestDataManagerIT;
import test.services.TestCacheService;
import test.services.TestSecurityService;

@TestPropertySource(locations="classpath:application.properties")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { DataServiceAutoConfiguration.class, TestDataManagerIT.class, TestCacheService.class, TestSecurityService.class })
public class DataServiceAutoConfigurationTestIT {

	@Autowired
	DataServiceAutoConfiguration dataServiceAutoConfiguration;

	@Test
	public void testDataService() throws Exception {
		DataService dataService = dataServiceAutoConfiguration.dataService();
		assertNotNull(dataService);
		DataManager dataManager = getDataManagerFromService((AbstractDataService) dataService);
		assertTrue(TestDataManagerIT.class.isInstance(dataManager));
		DataCacheService cacheService = ((TestDataManagerIT) dataManager).getDataCacheServiceTest();
		assertTrue(TestCacheService.class.isInstance(cacheService));
		DataSecurityService securityService = ((TestDataManagerIT) dataManager).getDataSecurityServiceTest();
		assertTrue(TestSecurityService.class.isInstance(securityService));
	}
	
	private DataManager getDataManagerFromService(AbstractDataService service) throws Exception {
		Field privateField = AbstractDataService.class.getDeclaredField("dataManager");
		privateField.setAccessible(true);
		return (DataManager) privateField.get(service);
	}

}
