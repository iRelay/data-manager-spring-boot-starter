package net.relaysoft.commons.data.conf;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.mock.env.MockEnvironment;

import net.relaysoft.commons.data.Data;
import net.relaysoft.commons.data.conf.DataManagerProperties.Cache;
import net.relaysoft.commons.data.conf.DataManagerProperties.Security;
import net.relaysoft.commons.data.enums.DataPersistenceEnum;
import net.relaysoft.commons.data.manager.DataManager;
import net.relaysoft.commons.data.manager.InMemoryDataManager;
import net.relaysoft.commons.data.services.AbstractDataService;
import net.relaysoft.commons.data.services.DataCacheService;
import net.relaysoft.commons.data.services.DataSecurityService;
import net.relaysoft.commons.data.services.DataService;
import test.managers.TestDataManager;
import test.services.TestCacheService;
import test.services.TestSecurityService;

@RunWith(MockitoJUnitRunner.class)
public class DataServiceAutoConfigurationTest {
	
	@Mock
	private ApplicationContext context;

	@Mock
	private DataManagerProperties dataManagerProperties;

	@Mock
	private MockEnvironment env;
	
	@Mock
	private Cache mockCache;
	
	@Mock
	private Security mockSecurity;
	
	@InjectMocks
	private DataServiceAutoConfiguration dataServiceAutoConfiguration = spy(new DataServiceAutoConfiguration());
	
	private DataManager testDataManager;

	@Before
	public void setUp() throws Exception {
		testDataManager = new TestDataManager();
		Map<String, Object> map = new HashMap<>();
		map.put("prop1", "value1");
		MutablePropertySources propertySources = new MutablePropertySources();
		propertySources.addLast(new MapPropertySource("prop1", map));
		when(env.getPropertySources()).thenReturn(propertySources);
		when(env.getProperty("prop1")).thenReturn("value1");
		when(dataManagerProperties.getSecurity()).thenReturn(mockSecurity);
		when(dataManagerProperties.getCache()).thenReturn(mockCache);
	}
	
	@Test
	public void testDataServiceWhenDefaultBean() throws Exception {
		when(context.getBean(DataManager.class)).thenReturn(testDataManager);
		assertDataManager();
	}
	
	@Test
	public void testDataServiceWhenBeanByName() throws Exception {
		when(dataManagerProperties.getName()).thenReturn("xxx");
		when(context.getBean("xxx")).thenReturn(testDataManager);
		assertDataManager();
	}

	@Test
	public void testDataServiceWhenBeanByClass() throws Exception {
		when(dataManagerProperties.getName()).thenReturn(testDataManager.getClass().getName());
		when(context.getBean(testDataManager.getClass())).thenAnswer(new Answer<DataManager>() {
			public DataManager answer(InvocationOnMock invocation) throws Throwable {
			    return testDataManager;
			  }
		});
		assertDataManager();
	}
	
	@Test
	public void testDataServiceWhenPojoByClass() throws Exception {
		when(dataManagerProperties.getName()).thenReturn(testDataManager.getClass().getName());
		assertDataManager();
	}
	
	@Test
	public void testDataServiceWhenNoName() throws Exception {
		DataService service = dataServiceAutoConfiguration.dataService();
		assertNotNull(service);
		DataManager dataManager = getDataManagerFromService((AbstractDataService) service);
		assertTrue(InMemoryDataManager.class.isInstance(dataManager));
		Data data = service.createByteData(DataPersistenceEnum.PERSISTENT);
		assertNotNull(data);
		assertTrue(data.getDataID().getInstanceId() > 0);
	}
	
	@Test
	public void testDataServiceWithCustomCacheService() throws Exception {
		when(dataManagerProperties.getName()).thenReturn(testDataManager.getClass().getName());
		when(mockCache.getName()).thenReturn(TestCacheService.class.getName());
		DataService service = dataServiceAutoConfiguration.dataService();
		assertNotNull(service);
		DataManager dataManager = getDataManagerFromService((AbstractDataService) service);
		assertTrue(InMemoryDataManager.class.isInstance(dataManager));
		DataCacheService cacheService = ((TestDataManager) dataManager).getDataCacheService();
		Properties properties = ((TestCacheService) cacheService).getProperties();
		assertEquals("value1", properties.getProperty("prop1"));
	}
	
	@Test
	public void testDataServiceWithCustomSecurityService() throws Exception {
		when(dataManagerProperties.getName()).thenReturn(testDataManager.getClass().getName());
		when(mockSecurity.getName()).thenReturn(TestSecurityService.class.getName());
		DataService service = dataServiceAutoConfiguration.dataService();
		assertNotNull(service);
		DataManager dataManager = getDataManagerFromService((AbstractDataService) service);
		assertTrue(InMemoryDataManager.class.isInstance(dataManager));
		DataSecurityService securityService = ((TestDataManager) dataManager).getDataSecurityService();
		Properties properties = ((TestSecurityService) securityService).getProperties();
		assertEquals("value1", properties.getProperty("prop1"));
	}
	
	private void assertDataManager() throws Exception {
		DataService service = dataServiceAutoConfiguration.dataService();
		assertNotNull(service);
		DataManager dataManager = getDataManagerFromService((AbstractDataService) service);
		assertTrue(testDataManager.getClass().isInstance(dataManager));
		Data data = service.createByteData(DataPersistenceEnum.PERSISTENT);
		assertNotNull(data);
		assertTrue(data.getDataID().getInstanceId() > 0);
	}
	
	private DataManager getDataManagerFromService(AbstractDataService service) throws Exception {
		Field privateField = AbstractDataService.class.getDeclaredField("dataManager");
		privateField.setAccessible(true);
		return (DataManager) privateField.get(service);
	}

}
