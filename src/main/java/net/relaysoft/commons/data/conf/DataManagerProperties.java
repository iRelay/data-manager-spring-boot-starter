package net.relaysoft.commons.data.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "relaysoft.data.manager")
public class DataManagerProperties {

	public static class Cache {
		
		private String name;
		
		public String getName() {
			return name;
		}
		
		public void setName(String name) {
			this.name = name;
		}
		
	}
	
	public static class Security {
		
		private String name;
		private String encryptionStrategy;
		
		public String getEncryptionStrategy() {
			return encryptionStrategy;
		}
		
		public String getName() {
			return name;
		}
		
		public void setEncryptionStrategy(String encryptionStrategy) {
			this.encryptionStrategy = encryptionStrategy;
		}
		
		public void setName(String name) {
			this.name = name;
		}		
		
	}
	
	private String name;
	private String instanceId;
	private String lockTimeout;
	
	private Cache cache;
	private Security security;

	public Cache getCache() {
		return cache;
	}
	
	public String getInstanceId() {
		return instanceId;
	}

	public String getLockTimeout() {
		return lockTimeout;
	}

	public String getName() {
		return name;
	}

	public Security getSecurity() {
		return security;
	}

	public void setCache(Cache cache) {
		this.cache = cache;
	}
	
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public void setLockTimeout(String lockTimeout) {
		this.lockTimeout = lockTimeout;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}
		
}
